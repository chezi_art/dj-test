import {Directive, ElementRef, EventEmitter, HostListener, Input, Output} from '@angular/core';

@Directive({
  selector: '[appCheckScroll]'
})
export class CheckScrollDirective {
  @Input() checkLoad: boolean;
  @Output() scroll: EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor() {
  }

  @HostListener('document:scroll', ['$event']) onScroll($event) {
    $event.preventDefault();
    let element = event.target['scrollingElement'];
    let checkScroll = (element.scrollHeight - element.scrollTop) < (element.clientHeight+100);
    if(this.checkLoad && checkScroll) {
      this.scroll.emit(true);
    }
  }
}
