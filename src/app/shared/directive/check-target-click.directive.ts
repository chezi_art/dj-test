import {Directive, ElementRef, EventEmitter, HostListener, Output} from '@angular/core';

@Directive({
  selector: '[appCheckTargetClick]'
})
export class CheckTargetClickDirective {
  hook: boolean = false;
  @Output() hide = new EventEmitter();

  constructor(private eRef: ElementRef,) { }
  @HostListener('document:click', ['$event'])
  clickOut(event) {
      this.hook = !this.hook;
      this.hide.emit(this.hook);
  }
}
