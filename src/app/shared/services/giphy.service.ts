import {Injectable} from '@angular/core';
import {GiphyFetch} from '@giphy/js-fetch-api'
import {environment} from "../../../environments/environment";
import {promise} from "selenium-webdriver";
import {GifsResult, GifResult} from "@giphy/js-fetch-api/src/result-types";
import {OptionsUpload} from "../interfaces/options-upload";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {ToastrService} from "ngx-toastr";
import {message} from "../constants/message.constant";


const gf = new GiphyFetch(environment.giphyKey);

@Injectable({
  providedIn: 'root'
})
export class GiphyService {

  limit: number = 25;
  uploadUrl: string = `https://upload.giphy.com/v1/gifs?api_key=${environment.giphyKey}`;

  constructor(private http: HttpClient,
              private toastrService: ToastrService) {
  }

  async getTrendGif(offset: number = 0): Promise<GifsResult> {
    try {
      return await gf.trending({offset: offset, limit: this.limit});
    } catch (e) {
      this.toastrService.error(message.ERROR);
    }
  }

  async getGifs(ids: string[]): Promise<GifsResult> {
    try {
      return await gf.gifs(ids);
    } catch (e) {
      this.toastrService.error(message.ERROR);
    }
  }

  async searchCategories(category: string, subcategory: string): Promise<GifsResult> {
    try {
      return await gf.gifs(category, subcategory);
    } catch (e) {
      this.toastrService.error(message.ERROR);
    }
  }

  async searchByTerm(term: string, offset: number = 0): Promise<GifsResult> {
    try {
      return await gf.search(term, {offset: offset, limit: this.limit});
    } catch (e) {
      this.toastrService.error(message.ERROR);
    }
  }

  async getLogo(id: string = 'z2POQZ2t8s5Gzp4C7r'): Promise<GifResult> {
    try {
      return await gf.gif(id);
    } catch (e) {
      this.toastrService.error(message.ERROR);
    }
  }

  upload(postObj: FormData): Observable<any> {
    try {
      return this.http.post<any>(this.uploadUrl, postObj);
    } catch (e) {
      this.toastrService.error(message.ERROR);
    }
  }

  async getRandom(): Promise<GifResult> {
    try {
      return await gf.random();
    } catch (e) {
      this.toastrService.error(message.ERROR);
    }
  }

  async getGif(id: string): Promise<GifResult> {
    try {
      return await gf.gif(id);
    } catch (e) {
      this.toastrService.error(message.ERROR);
    }
  }
}
