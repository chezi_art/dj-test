import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  constructor() { }
  showErrorsMessage(control: any, controlName: string) {
    if (control.errors.required || control.errors.validName) {
      return controlName + ' is required';
    } else if (control.errors.minlength || control.errors.maxlength ||control.errors.pattern ) {
      return 'Password must be 6 to 24 characters and have at least 1 number';
    } else if(control.errors.validType) {
      return  'Type file must be gif';
    } else {
      return;
    }
  }
}
