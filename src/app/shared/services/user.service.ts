import { Injectable } from '@angular/core';
import {User} from "../interfaces/user";
import {UserModel} from "../models/user.model";
import {Users} from "../interfaces/users";
import {UserToUsersModel} from "../models/user-to-users.model";
import {ToastrService} from "ngx-toastr";
import {message} from "../constants/message.constant";
import {Router} from "@angular/router";
import {GiphyService} from "./giphy.service";
import {promise} from "selenium-webdriver";
import {GifResult} from "@giphy/js-fetch-api";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  user: User;
  userAvatar: string;

  constructor(private toastrService: ToastrService, private router: Router,
              private gS: GiphyService) {
    this.getUser()
  }

  getUser(): void {
    this.user = JSON.parse(localStorage.getItem('user')) || undefined;
    if(!!this.user) {
      this.gS.getGif(this.user.gif_id).then((gif: GifResult) => {
        this.userAvatar = !!gif.data.images['preview_webp'] && gif.data.images['preview_webp'].url ?
          gif.data.images['preview_webp'].url : gif.data.images.downsized.url;
      })
    }
  }

  async singUp(name: string, password: string): Promise<void> {
    let gif = await this.gS.getRandom();
    let user: User = new UserModel(name, [], gif.data.id.toString());
    let users: Users = new UserToUsersModel(name, password, [],gif.data.id.toString());
    let usersStore: Users[] = JSON.parse(localStorage.getItem('users'));
    if(!!usersStore) {
      let index: number = usersStore.findIndex(indexArr=> indexArr.name === name);
      index === -1 ? this.saveUsers(usersStore, user, users) : this.toastrService.warning(message.NAME_EXISTS);
    } else {
      this.saveUsers([users], user, users);
    }
  }

  singIn(name: string, password: string): void {
    let usersStore: Users[] = JSON.parse(localStorage.getItem('users'));
    if(!!usersStore) {
      let index: number = usersStore.findIndex(indexArr=> indexArr.name === name && indexArr.password === password);
      index !== -1 ? this.saveUser(usersStore[index]) : this.toastrService.warning(message.USER_NOT_FOUND);
    } else {
      this.toastrService.warning(message.USER_NOT_FOUND);
    }
  }

  saveUsers(usersStore: Users[], user: User, users: Users): void {
    localStorage.setItem('user', JSON.stringify(user));
    usersStore.push(users);
    localStorage.setItem('users', JSON.stringify(usersStore));
    this.getUser();
  }

  saveUser(user: Users): void {
    delete user.password;
    localStorage.setItem('user', JSON.stringify(user));
    this.getUser();
  }

  logOut(): void {
    let usersStore: Users[] = JSON.parse(localStorage.getItem('users'));
    let index: number = usersStore.findIndex(indexArr=> indexArr.name === this.user.name);
    usersStore[index].collection = this.user.collection;
    usersStore[index].gif_id = this.user.gif_id;
    localStorage.setItem('users', JSON.stringify(usersStore));
    localStorage.removeItem('user');
    this.getUser();
    this.router.navigate(['/dashboard/main']);
  }

  saveIdGif(id: string): void {
    this.user.collection.push(id);
    localStorage.setItem('user', JSON.stringify(this.user));
  }

}
