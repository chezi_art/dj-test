export interface User {
  name: string,
  collection: string[],
  gif_id: string
}
