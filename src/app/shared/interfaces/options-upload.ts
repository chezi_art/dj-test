export interface OptionsUpload {
  username: string,
  file: File,
  tags: string
}
