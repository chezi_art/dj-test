import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ValidationService} from "../../services/validation.service";
import {UserService} from "../../services/user.service";
import {ToastrService} from "ngx-toastr";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {CustomValidators} from "../../validators/validators";
import {message} from "../../constants/message.constant";

@Component({
  selector: 'app-user-sing-in',
  templateUrl: './user-sing-in.component.html',
  styleUrls: ['./user-sing-in.component.scss']
})
export class UserSingInComponent implements OnInit {
  sigInForm: FormGroup;
  constructor(private formBuilder: FormBuilder,
              private validService: ValidationService,
              private userService: UserService,
              private toastrService: ToastrService,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<UserSingInComponent>) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm(): void {
    this.sigInForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required, CustomValidators.validName])],
      password: ['', Validators.compose([Validators.required,
        Validators.minLength(6),
        CustomValidators.validName, Validators.maxLength(24)])]
    });
  }

  onSubmit() {
    if (this.sigInForm.valid) {
      this.userService.singIn(this.sigInForm.controls.name.value, this.sigInForm.controls.password.value);
      this.dialogRef.close();
    } else {
      this.toastrService.warning(message.CHECK_FIELDS);
    }
  }

}
