import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ValidationService} from "../../services/validation.service";
import {UserService} from "../../services/user.service";
import {ToastrService} from "ngx-toastr";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {CustomValidators} from "../../validators/validators";
import {message} from "../../constants/message.constant";
import {GiphyService} from "../../services/giphy.service";

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {
  uploadForm: FormGroup;
  fileName: string = '';
  disabled: boolean = false;

  constructor(private formBuilder: FormBuilder,
              private validService: ValidationService,
              private gS: GiphyService,
              private toastrService: ToastrService,
              private userService: UserService,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<UploadComponent>) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm(): void {
    this.uploadForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required, CustomValidators.validName])],
      file: ['', Validators.compose([Validators.required, CustomValidators.validType])],
      tags: ['', Validators.compose([Validators.required, CustomValidators.validName])],
    });
  }

  saveImage(ev: Event) {
    const file: File = ev[0];
    this.fileName = file.name;
    this.uploadForm.patchValue({file: file})
  }

  onSubmit() {
    if (this.uploadForm.valid) {
      this.disabled = true;
      let formData: FormData = new FormData();
      Object.keys(this.uploadForm.controls).forEach(key => {
        formData.append(key, this.uploadForm.controls[key].value);
      });
      this.gS.upload(formData).subscribe(res => {
        this.userService.saveIdGif(res.data.id);
        this.dialogRef.close(true);
      });
    } else {
      this.toastrService.warning(message.CHECK_FIELDS);
    }
  }

}
