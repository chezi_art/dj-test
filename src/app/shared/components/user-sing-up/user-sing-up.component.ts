import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CustomValidators} from "../../validators/validators";
import {ValidationService} from "../../services/validation.service";
import {UserService} from "../../services/user.service";
import {ToastrService} from "ngx-toastr";
import {message} from "../../constants/message.constant";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-user-sing-up',
  templateUrl: './user-sing-up.component.html',
  styleUrls: ['./user-sing-up.component.scss']
})
export class UserSingUPComponent implements OnInit {
  sigUpForm: FormGroup;
  constructor(private formBuilder: FormBuilder,
              private validService: ValidationService,
              private userService: UserService,
              private toastrService: ToastrService,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<UserSingUPComponent>) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm(): void {
    this.sigUpForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required, CustomValidators.validName])],
      password: ['', Validators.compose([Validators.required,
        Validators.minLength(6),
        CustomValidators.validName, Validators.maxLength(24)])],
      confirmPassword: ['', Validators.compose([Validators.required, CustomValidators.validName])]
    }, {validators: CustomValidators.checkPasswords});
  }

  checkConfirmError() {
    if(this.sigUpForm.controls.password && this.sigUpForm.controls.confirmPassword) {
      return this.sigUpForm.controls.password.value === this.sigUpForm.controls.confirmPassword.value ? null : 'Passwords don’t match';
    }
    return null;
  }

  onSubmit() {
    if (this.sigUpForm.valid) {
      console.log(1);
      this.userService.singUp(this.sigUpForm.controls.name.value, this.sigUpForm.controls.password.value);
      this.dialogRef.close();
    } else {
      this.toastrService.warning(message.CHECK_FIELDS);
    }
  }
}
