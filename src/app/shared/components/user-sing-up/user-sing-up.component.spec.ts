import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSingUPComponent } from './user-sing-up.component';

describe('UserSingUPComponent', () => {
  let component: UserSingUPComponent;
  let fixture: ComponentFixture<UserSingUPComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserSingUPComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSingUPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
