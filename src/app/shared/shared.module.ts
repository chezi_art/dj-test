import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImagePopUpComponent } from './components/image-pop-up/image-pop-up.component';
import {CheckScrollDirective} from "./directive/check-scroll.directive";
import { UserSingInComponent } from './components/user-sing-in/user-sing-in.component';
import { UserSingUPComponent } from './components/user-sing-up/user-sing-up.component';
import { ConfirmComponent } from './components/confirm/confirm.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { UploadComponent } from './components/upload/upload.component';
import { CheckTargetClickDirective } from './directive/check-target-click.directive';

@NgModule({
  declarations: [
    ImagePopUpComponent,
    CheckScrollDirective,
    UserSingInComponent,
    UserSingUPComponent,
    ConfirmComponent,
    UploadComponent,
    CheckTargetClickDirective,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    CheckScrollDirective,
    CheckTargetClickDirective,
  ],
  entryComponents:[
    ImagePopUpComponent,
    UserSingInComponent,
    UserSingUPComponent,
    ConfirmComponent,
    UploadComponent,
  ]
})
export class SharedModule { }
