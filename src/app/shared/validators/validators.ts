import {FormControl, FormGroup} from '@angular/forms';

export class CustomValidators {

  constructor() {
  }

  /*Valid trim*/
  public static validName(control: FormControl) {
    if (control.value) {
      return control.value && control.value.trim() ? null : {validName: 'Enter value'};
    }
    return null;
  }

  /*Valid confirm password*/
  public static checkPasswords(group: FormGroup) {
    let pass = group.get('password').value;
    let confirmPass = group.get('confirmPassword').value;
    return pass === confirmPass ? null : {checkPasswords: 'Passwords don’t match'}
  }

  /*Valid type file*/
  public static validType(control: FormControl) {
    if (control.value) {
      let type = control.value.name.split('.')[1];
      return (type === 'gif') ? null : {validType: 'Type file must be gif'};
    }
    return null;
  }

}
