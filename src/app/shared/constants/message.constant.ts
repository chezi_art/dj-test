export const message = {
  NO_LOGIN: 'You need login before',
  LOAD_ALL: 'You loaded all gifs',
  CHECK_FIELDS: 'Please check fields',
  NAME_EXISTS: 'This name exists',
  GIF_EXISTS: 'This gif exists in your collection',
  USER_NOT_FOUND: 'User not found',
  ERROR: 'Service not available now. Please try later',
  CONFIRM_ADD: 'Do you want to add it in your collection?',
  CONFIRM_DELETE: 'Do you want to delete this gif?',
  CONFIRM_CHANGE_AVATAR: 'Do you want to change avatar?'
};
