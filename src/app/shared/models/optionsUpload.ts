export class OptionsUpload {
  constructor(
    public username: string,
    public file: File,
    public tags: string
  ) {}
}
