export class UserToUsersModel {
  constructor(public name: string,
              public password: string,
              public collection: string[],
              public gif_id: string) {
  }
}
