import { Component, OnInit } from '@angular/core';
import {GifsResult} from "@giphy/js-fetch-api";
import {GiphyService} from "../../shared/services/giphy.service";
import {MatDialog} from "@angular/material";
import {ImagePopUpComponent} from "../../shared/components/image-pop-up/image-pop-up.component";
import {ToastrService} from "ngx-toastr";
import {message} from "../../shared/constants/message.constant";
import {Router} from "@angular/router";
import {UserService} from "../../shared/services/user.service";
import {ConfirmComponent} from "../../shared/components/confirm/confirm.component";
import {debounceTime, distinctUntilChanged} from "rxjs/operators";
import {Subject} from "rxjs";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  gifs: GifsResult;
  checkLoad: boolean = true;
  searchModelText: string;
  searchInput: Subject<string> = new Subject<string>();

  constructor(private gS: GiphyService,
              public dialog: MatDialog,
              private toastrService: ToastrService,
              private router: Router,
              private userService: UserService) {
    this.getData();
  }

  ngOnInit() {
    this.getGifsTerm();
  }

  async getData(): Promise<void> {
    this.gifs = await this.gS.getTrendGif();
  }

  showGif(url: string, title: string): void {
    this.dialog.open(ImagePopUpComponent, {
      data: {url: url, title: title},
      hasBackdrop: true,
    });
  }

  addGif(id: string): void {
    let dialogRef = this.dialog.open(ConfirmComponent, {
      data: {text: message.CONFIRM_ADD},
      hasBackdrop: true,
    });

    let index = this.userService.user.collection.findIndex(indexArr => indexArr === id);

    dialogRef.afterClosed().subscribe((res: boolean)=>{
      if(res && index === -1) {
        !!this.userService.user ? this.saveIdGif(id) : this.toastrService.warning(message.NO_LOGIN);
      } else if(index !== -1) {
        this.toastrService.warning(message.GIF_EXISTS);
      }
    });
  }

  saveIdGif(id: string): void {
    this.userService.saveIdGif(id);
  }

  checkScroll(ev: boolean): void {
   if(ev) {
     this.checkLoad = false;
     this.loadNextGifs();
   }
  }

  async loadNextGifs(): Promise<void> {
    if(this.gifs.pagination.total_count > this.gifs.data.length) {
      console.log(this.searchModelText);
      let gifs = !!this.searchModelText ? await this.gS.searchByTerm(this.searchModelText, this.gifs.pagination.offset + this.gS.limit) :
        await this.gS.getTrendGif(this.gifs.pagination.offset + this.gS.limit);
      this.gifs.data = this.gifs.data.concat(gifs.data);
      this.gifs.pagination = gifs.pagination;
      this.checkLoad = true;
    } else {
      this.toastrService.warning(message.LOAD_ALL);
    }
  }

  search(text: string): void {
    this.searchInput.next(text);
  }

  getGifsTerm() {
    this.searchInput.pipe(debounceTime(300), distinctUntilChanged()).subscribe(async (term: string) => {
      this.gifs = null;
      this.gifs = !!term ? await this.gS.searchByTerm(term) : await this.gS.getTrendGif();
    });
  }

}
