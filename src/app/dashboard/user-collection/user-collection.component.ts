import { Component, OnInit } from '@angular/core';
import {GifsResult} from "@giphy/js-fetch-api";
import {GiphyService} from "../../shared/services/giphy.service";
import {UserService} from "../../shared/services/user.service";
import {ImagePopUpComponent} from "../../shared/components/image-pop-up/image-pop-up.component";
import {MatDialog} from "@angular/material";
import {UploadComponent} from "../../shared/components/upload/upload.component";
import {ConfirmComponent} from "../../shared/components/confirm/confirm.component";
import {message} from "../../shared/constants/message.constant";

@Component({
  selector: 'app-user-collection',
  templateUrl: './user-collection.component.html',
  styleUrls: ['./user-collection.component.scss']
})
export class UserCollectionComponent implements OnInit {

  gifs: GifsResult;

  constructor(private gS: GiphyService, private userService: UserService,
              public dialog: MatDialog,) {
    this.getData();
  }

  ngOnInit() {
  }
  deleteGif(id: string): void {
    let dialogRef = this.dialog.open(ConfirmComponent, {
      data: {text: message.CONFIRM_DELETE},
      hasBackdrop: true,
    });

    dialogRef.afterClosed().subscribe((res: boolean)=>{
      if(res) {
        this.deleteGifFromCollection(id);
      }
    });
  }

  changeAvatar(id: string): void {
    let dialogRef = this.dialog.open(ConfirmComponent, {
      data: {text: message.CONFIRM_CHANGE_AVATAR},
      hasBackdrop: true,
    });

    dialogRef.afterClosed().subscribe((res: boolean)=>{
      if(res) {
        this.userService.user.gif_id = id;
        localStorage.setItem('user', JSON.stringify(this.userService.user));
        this.userService.getUser();
      }
    });
  }

  deleteGifFromCollection(id: string): void {
    let index = this.userService.user.collection.findIndex(indexArr=> indexArr === id);
    if(index !== -1) {
      this.userService.user.collection.splice(index, 1);
      localStorage.setItem('user', JSON.stringify(this.userService.user));
      this.getData();
    }
  }

  async getData(): Promise<void> {
    if(this.userService.user.collection.length > 0) {
      this.gifs = await this.gS.getGifs(this.userService.user.collection);
    }
  }

  showGif(url: string, title: string): void {
    this.dialog.open(ImagePopUpComponent, {
      data: {url: url, title: title},
      hasBackdrop: true,
    });
  }

  upload(): void {
    let dialogRef = this.dialog.open(UploadComponent, {
      hasBackdrop: true,
    });

    dialogRef.afterClosed().subscribe((data: boolean)=> {
      if(!!data) {
        this.getData();
      }
    })
  }
}
