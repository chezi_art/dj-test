import { Component, OnInit } from '@angular/core';
import {Users} from "../shared/interfaces/users";
import {User} from "../shared/interfaces/user";
import {GiphyService} from "../shared/services/giphy.service";
import {GifResult} from "@giphy/js-fetch-api";
import {UserService} from "../shared/services/user.service";
import {MatDialog} from "@angular/material";
import {ConfirmComponent} from "../shared/components/confirm/confirm.component";
import {UserSingUPComponent} from "../shared/components/user-sing-up/user-sing-up.component";
import {message} from "../shared/constants/message.constant";
import {Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {UserSingInComponent} from "../shared/components/user-sing-in/user-sing-in.component";


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  logo: GifResult;

  showPhoneMenu: boolean = false;
  defaultGIf: string;
  currentUrl: string;

  constructor(private gS: GiphyService, private userService: UserService,
              public dialog: MatDialog, private router: Router,
              private toastrService: ToastrService) {
    this.getData();
  }

  ngOnInit() {
    this.getCurrentUrl();
  }

  getCurrentUrl(): void {
    this.currentUrl = this.router.url.split('/')[2];
    this.router.events.subscribe( (ev)=> {
      this.currentUrl = this.router.url.split('/')[2];
    })
  }

  async getData(): Promise<void> {
    this.logo = await this.gS.getLogo();
  }

  singIn(): void {
    let dialogRef = this.dialog.open(UserSingInComponent, {
      hasBackdrop: true,
    });
  }

  singUp(): void {
    let dialogRef = this.dialog.open(UserSingUPComponent, {
      hasBackdrop: true,
    });
  }

  logOut(): void {
    this.userService.logOut()
  }

  goToMyCollection(): void {
    !!this.userService.user ? this.router.navigate(['dashboard/user-collection']) : this.toastrService.warning(message.NO_LOGIN);
  }
}
