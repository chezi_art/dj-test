import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DashboardRoutingModule} from "./dashboard-routing.module";
import {DashboardComponent} from "./dashboard.component";
import { MainComponent } from './main/main.component';
import { UserCollectionComponent } from './user-collection/user-collection.component';
import {SharedModule} from "../shared/shared.module";
import {MatProgressSpinnerModule} from "@angular/material";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    DashboardComponent,
    MainComponent,
    UserCollectionComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
    MatProgressSpinnerModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class DashboardModule { }
