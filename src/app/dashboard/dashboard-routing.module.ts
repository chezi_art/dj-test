import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {DashboardComponent} from "./dashboard.component";
import {MainComponent} from "./main/main.component";
import {UserCollectionComponent} from "./user-collection/user-collection.component";
import {AuthGuard} from "../shared/guards/auth.guard";

const routes: Routes = [{
    path: '', component: DashboardComponent,
    children: [
      {path: '', redirectTo: 'main'},
      {path: 'main', component: MainComponent},
      {path: 'user-collection', component: UserCollectionComponent, canActivate: [AuthGuard]}
    ]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
