import { Component } from '@angular/core';
import {GiphyService} from "./shared/services/giphy.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'DjTest';
  constructor(protected gS: GiphyService) {
  }
}

